using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Training.Manager
{
    public class ScoreManager : MonoBehaviour
    {
        public static ScoreManager instance = null;
        [HideInInspector] public int score = 0;
        [SerializeField] private TMP_Text scoreText;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        void Update()
        {
            scoreText.text = "Score: " + score.ToString();
        }
    }
}
