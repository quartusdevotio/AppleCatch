using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Training.Manager
{
    public class AudioManager : MonoBehaviour
    {
        public static AudioManager instance = null;
        [SerializeField] private Sound[] music, sfx;
        [SerializeField] private AudioSource musicSource, sfxSource;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            SetMusicVolume(0.7f);
            PlayMusic("Omurice");
        }

        public void PlayMusic(string name)
        {
            Sound sound = Array.Find(music, x => x.name == name);

            if (sound != null)
            {
                musicSource.clip = sound.clip;
                musicSource.Play();
            }
        }

        public void PlaySfx(string name, float volume)
        {
            Sound sound = Array.Find(sfx, x => x.name == name);

            if (sound != null)
            {
                sfxSource.clip = sound.clip;
                sfxSource.PlayOneShot(sfxSource.clip, volume);
            }
        }

        public void SetMusicVolume(float volume)
        {
            musicSource.volume = volume;
        }
    }
}
