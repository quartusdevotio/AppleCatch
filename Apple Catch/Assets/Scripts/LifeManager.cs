using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Training.Manager
{
    public class LifeManager : MonoBehaviour
    {
        public static LifeManager instance = null;
        private int _life = 3;
        public int Life { get; }
        [SerializeField] private Transform platformsTransform;
        [SerializeField] private Vector2[] targetPositions;
        [SerializeField] private float speed = 1f;

        [HideInInspector] public bool isDead = false;
        [SerializeField] private GameObject appleSpawner;
        [SerializeField] private GameObject backgroundFlame;
        [SerializeField] private GameObject gameOverCanvas;

        public delegate void PlayerDeadEventHandler();
        public event PlayerDeadEventHandler PlayerDead;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (!isDead)
            {
                float step = speed * Time.deltaTime;
                platformsTransform.transform.position = Vector2.MoveTowards(platformsTransform.transform.position, targetPositions[_life], step);
            }
        }

        public void SubtractLife(int damage)
        {
            if (_life == 0)
            {
                return;
            }

            _life -= damage;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                isDead = true;
                PlayerDead();
                Destroy(appleSpawner);
                Destroy(backgroundFlame);
                Destroy(platformsTransform.gameObject);
                Destroy(gameObject.GetComponent<EdgeCollider2D>());
                gameOverCanvas.SetActive(true);
            }
        }
    }
}