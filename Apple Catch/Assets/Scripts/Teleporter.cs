using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Training
{
    public class Teleporter : MonoBehaviour
    {
        [SerializeField] private float targetPositionY;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                Vector2 targetPosition = new Vector2(collision.transform.position.x, targetPositionY);
                collision.transform.position = targetPosition;
            }
        }
    }
}