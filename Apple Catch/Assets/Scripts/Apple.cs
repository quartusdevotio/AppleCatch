using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Training.Manager;

namespace Training
{
    public class Apple : MonoBehaviour
    {
        private Rigidbody2D rigidBody2D;
        [SerializeField] private float fallMultiplier = 0.2f;

        private enum AppleType
        {
            NORMAL,
            GOLDEN,
        };
        [SerializeField] private AppleType appleType;

        // Start is called before the first frame update
        private void Start()
        {
            rigidBody2D = GetComponent<Rigidbody2D>();
        }

        private void FixedUpdate()
        {
            rigidBody2D.velocity += Vector2.up * Physics2D.gravity.y * fallMultiplier * Time.deltaTime;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                if (appleType == AppleType.GOLDEN)
                {
                    ScoreManager.instance.score += 2;
                }
                else
                {
                    ScoreManager.instance.score += 1;
                }
                AudioManager.instance.PlaySfx("PickUp", 1f);
                Destroy(gameObject);
            }
            else if (collision.CompareTag("ItemDestroyer"))
            {
                if (appleType == AppleType.GOLDEN)
                {
                    AudioManager.instance.PlaySfx("DepleteLife", 1f);
                    LifeManager.instance.SubtractLife(1);
                }
                Destroy(gameObject);
            }
        }
    }
}
