using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Training.Manager;

namespace Training.UI
{
    public class UIController : MonoBehaviour
    {
        [SerializeField] private Slider musicSlider;

        public void ChangeMusicVolumeUBEvent()
        {
            AudioManager.instance.SetMusicVolume(musicSlider.value);
        }

        public void ReloadSceneUBEvent()
        {
            var activeScene = SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene(activeScene);
            AudioManager.instance.PlayMusic("Omurice");
        }
    }
}