using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Training.Manager
{
    public class ItemSpawner : MonoBehaviour
    {
        private static float YPOS = 4.5f;
        [SerializeField] private GameObject[] itemPrefab;
        [SerializeField] private float minimumOffset;
        [SerializeField] private float maximumOffset;
        [SerializeField] private float cooldown = 1f;
        [SerializeField] private bool stop;

        [SerializeField] private float[] percentages;

        void Start()
        {
            StartCoroutine(Spawn());
        }

        IEnumerator Spawn()
        {
            while (!stop)
            {
                Vector2 randomPos = new Vector2(Random.Range(minimumOffset, maximumOffset), YPOS);
                Instantiate(itemPrefab[RandomSpawn()], randomPos, Quaternion.identity);
                AudioManager.instance.PlaySfx("AppleSpawn", 0.3f);
                yield return new WaitForSeconds(cooldown);
            }
        }

        private int RandomSpawn()
        {
            int output = 0;
            float random = Random.Range(0f, 1f);
            float total = 0f;
            float addNum = 0f;

            for (int i = 0; i < percentages.Length; i++)
            {
                total += percentages[i];
            }

            for (int i = 0; i < itemPrefab.Length; i++)
            {
                if (percentages[i] / total + addNum >= random)
                {
                    output = i;
                    return output;
                }
                else
                {
                    addNum += percentages[i] / total;
                }
            }
            return 0;
        }
    }
}